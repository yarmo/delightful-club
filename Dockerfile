FROM rust:1.76.0 as builder

WORKDIR /app

COPY . .
RUN cargo build --release

###

FROM debian
WORKDIR /app

RUN apt-get update
RUN apt-get install -y ca-certificates

COPY --from=builder /app/target/release/delightfulclub /usr/local/bin