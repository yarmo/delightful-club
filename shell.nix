let
  pkgs = import (fetchTarball https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz) {};
in
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    rustc
    rust-analyzer
    cargo
    gcc
    rustfmt
    clippy
    pkg-config
    openssl
    httplz
  ];
  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
}
