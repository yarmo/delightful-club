use config::{ConfigError, Config, File};
use serde::Deserialize;

use crate::list::List;

/// Delightful club configuration
#[derive(Debug, Deserialize)]
pub(crate) struct Settings {
    pub(crate) lists: Vec<List>
}

impl Settings {
    pub(crate) fn new() -> Result<Self, ConfigError> {
        let mut s = Config::default();
        s.merge(File::with_name("config.toml"))?;
        s.try_into()
    }
}
