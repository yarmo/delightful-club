use askama::Template;
use comrak::ComrakOptions;

use crate::{list::List, CacheBuster};

#[derive(Template)]
#[template(path = "index.html")]
pub(crate) struct IndexTemplate<'a> {
    pub(crate) cachebuster: &'a CacheBuster,
    pub(crate) lists: &'a Vec<List>,
    pub(crate) content: &'a str,
}

#[derive(Template)]
#[template(path = "list.html")]
pub(crate) struct ListTemplate<'a> {
    pub(crate) cachebuster: &'a CacheBuster,
    pub(crate) list: &'a List,
    pub(crate) content: &'a str,
    pub(crate) options: &'a ComrakOptions,
}

#[derive(Template)]
#[template(path = "404.html")]
pub(crate) struct Http404Template<'a> {
    pub(crate) cachebuster: &'a CacheBuster,
}

#[derive(Template)]
#[template(path = "sitemap.xml")]
pub(crate) struct SitemapTemplate<'a> {
    pub(crate) lists: &'a Vec<List>,
}
