use std::{collections::HashMap, fs, io};
use askama::Template;
use comrak::{ComrakExtensionOptions, ComrakOptions};
use list::List;
use regex::Regex;
use sha2::{Sha256, Digest};
use base64ct::{Base64UrlUnpadded, Encoding};
use settings::Settings;

use crate::templates::{Http404Template, IndexTemplate, ListTemplate, SitemapTemplate};

pub(crate) mod settings;
pub(crate) mod templates;
pub(crate) mod list;

/// Render the index page
async fn render_index(cachebuster: &CacheBuster, lists: &Vec<List>) -> Result<(), Box<dyn std::error::Error>> {
    let content = "## About delightful lists

The delightful project is an initiative by [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary).

From [teaserbot-labs/delightful on codeberg](https://codeberg.org/teaserbot-labs/delightful):

> The internet can be a wonderful place. A place that unites people, to exchange ideas. It allows people to cooperate and unleash their creativity. This brought forth great source of information and knowledge, open science and many beautiful free software projects to enrich our lives with. The internet has the potential to bring our minds together. To set us free.
> 
> But increasingly commercial interests are thwarting the vision of what the internet can bring to humanity. The gems of free knowledge and open source software are harder to find these day. Drowned out by marketing, advertising and SEO they do not appear in the top of our search results anymore.
> 
> Delightful lists are an effort to help bring change to this trend. To make freedom more discoverable again. This top-level project will allow navigation to all high-quality curated delightful lists created and maintained all over the web.
> 
> Anyone that wishes to do so can create their own list, and thus create an entrypoint to freedom.
>
> [...]
>
> The whole concept of delightful lists was inspired by the popular awesome project on Github, that was started by [Sindre Sorhus](https://sindresorhus.com/). Read more in our FAQ.

## About delightful.club

[delightful.club](/) is maintained by [Yarmo](https://yarmo.eu), a big fan of delightful lists and FOSS in general.

The individual lists themselves are maintained by their respective authors.

This website itself is [open source](https://codeberg.org/yarmo/delightful-club) (hosted on [Codeberg.org](https://codeberg.org)) and dedicated to the public domain ([CC0-1.0 license](https://creativecommons.org/publicdomain/zero/1.0/)).

CSS framework: [Simple.css](https://simplecss.org/)  
Tracking: none";

    let template = IndexTemplate {
        cachebuster,
        lists,
        content,
    };

    fs::write("public/index.html", template.render()?)?;

    println!("index rendered");
    Ok(())
}

/// Render the 404 page
async fn render_404(cachebuster: &CacheBuster) -> Result<(), Box<dyn std::error::Error>> {
    let template = Http404Template {
        cachebuster,
    };

    fs::write("public/404.html", template.render()?)?;

    println!("404 rendered");
    Ok(())
}

/// Render the sitemap
async fn render_sitemap(lists: &Vec<List>) -> Result<(), Box<dyn std::error::Error>> {
    let template = SitemapTemplate {
        lists,
    };

    fs::write("public/sitemap.xml", template.render()?)?;

    println!("sitemap rendered");
    Ok(())
}

/// Render a delightful list
async fn render_list(cachebuster: &CacheBuster, list: &List) -> Result<(), Box<dyn std::error::Error>> {
    let slug = list.to_slug();

    let resp = reqwest::get(&list.source)
        .await?
        .text()
        .await?;

    let re_html_comment = Regex::new(r"<!--([^>]*)-->")?;
    let content = re_html_comment.replace(&resp, "");

    let options = &ComrakOptions {
        extension: ComrakExtensionOptions {
            autolink: true,
            header_ids: Some(String::new()),
            strikethrough: true,
            table: true,
            tagfilter: true,
            ..Default::default()
        },
        ..Default::default()
    };

    let template = ListTemplate {
        cachebuster,
        list,
        content: &content,
        options: &options,
    };

    fs::create_dir_all(format!("public/{}", slug))?;
    fs::write(format!("public/{}/index.html", &slug), template.render()?)?;

    println!("{} rendered", &slug);
    Ok(())
}

/// Keep track of hashes of static files
pub(crate) struct CacheBuster {
    pub(crate) hashes: HashMap<String, String>,
    pub(crate) fallback: String
}

impl CacheBuster {
    pub(crate) fn get(&self, name: &str) -> String {
        match self.hashes.get(name) {
            Some(hash) => hash.to_string(),
            None => self.fallback.clone(),
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let pattern = std::env::args().nth(1);

    if let Some(p) = pattern {
        if p == "-v" || p == "--version" {
            println!(env!("CARGO_PKG_VERSION"));
            return Ok(());
        }
    };

    let now = chrono::Utc::now();

    // Initialize cache buster
    let mut hasher = Sha256::new();
    hasher.update(now.to_rfc3339().as_bytes());
    let hash_bytes = hasher.finalize();
    let mut cb = CacheBuster { 
        hashes: HashMap::new(),
        fallback: Base64UrlUnpadded::encode_string(&hash_bytes[..16])
    };

    let settings = Settings::new();
    let mut lists = settings?.lists;
    lists.sort_by_key(|list| list.title.to_lowercase());

    // Remove the public directory
    let _ = fs::remove_dir_all("public");
    println!("public directory removed");
    fs::create_dir_all("public")?;

    // Handle static files
    for file in fs::read_dir("static")? {
        let file = file?;
        
        // Copy file
        fs::copy(file.path(), format!("public/{}", file.file_name().to_string_lossy()))?;

        // Hash file
        if let Some(name) = file.file_name().to_str() {
            let mut hasher = Sha256::new();
            let mut content = fs::File::open(file.path())?;
            let _bytes_written = io::copy(&mut content, &mut hasher)?;
            let hash_bytes = hasher.finalize();
            cb.hashes.insert(name.to_string(), Base64UrlUnpadded::encode_string(&hash_bytes[..16]));
        }
    }
    println!("static files copied");

    // Render the index
    render_index(&cb, &lists).await?;
    render_404(&cb).await?;
    render_sitemap(&lists).await?;

    // Render the lists
    for list in &lists {
        render_list(&cb, list).await?;
    }

    Ok(())
}
