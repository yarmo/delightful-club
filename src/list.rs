use serde::Deserialize;

/// Delightful list configuration
#[derive(Debug, Deserialize)]
pub(crate) struct List {
    pub title: String,
    pub source: String,
    pub repository: String,
}

impl List {
    /// Create a slug based on the list's title
    pub(crate) fn to_slug(&self) -> String {
        self.title.replace(" ", "_").to_lowercase()
    }
    /// Get the short name without "delightful "
    pub(crate) fn to_short_name(&self) -> String {
        self.title.replace("delightful ", "")
    }
}